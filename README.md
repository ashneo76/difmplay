difmplay
-------------
A fork of the original difmplay script written by Lasse Collin <lasse.collin@tukaani.org> at [difmplay homepage](http://tukaani.org/difmplay "difmplay homepage")

## About
difmplay is a shell script to ease playing music from Digitally Imported (DI.fm) and SKY.fm Internet radio. Both free and premium streams are supported. Playing the premium streams naturally requires a paid subscription to DI.fm or SKY.fm.

difmplay provides both command line and menu driven interfaces to select the channel and the bitrate. difmplay constructs an URL matching the choices, and passes the URL to the selected audio player application. This way one doesn't need to keep bookmarks of the playlist URLs for different channels and bitrates just to avoid visiting (and in case of premium, logging in) the DI.fm or SKY.fm website everytime one wants to play some music.

## Usage
`difmplay [option]... [channel]`

Channel may be omitted if interactive menu will be used or if the default channel has been specified in the configuration file.

Option | Meaning | Description
----|----|----
-b | bitrate | Set the bitrate as kbit/s for the premium subscription. 256 is MP3. 128, 64, and 40 are AAC. The default is 256. If the premium ID is not set, this option is ignored and the free stream is played.
-i | premium | Set the premium ID (a hexadecimal string) needed to construct URLs for premium streams. You can find this string by looking at the URLs of the channels when logged in DI.fm or SKY.fm website.
-p | player | Set the command to use as the audio player. It has to accept an URL to the playlist (.pls) as the last argument. Wordsplitting is applied to player, which makes it possible to pass additional command line options to the player program. The default is mplayer -playlist.
-m | | Display a menu using dialog to select the channel and bitrate. The default selections can be specified in the config file or on the command line.
-n | | Don't display a menu even if config file has MENU=yes or the -m option was already used.
-l | | Display the list of available channels. Multi-column output is used when printing to a terminal. Otherwise single-column output is used.
-u | | Download a new channel list to ~/.difmplay_channels. This may not work if the web site formatting on www.di.fm or www.sky.fm has changed. Delete ~/.difmplay_channels or run difmplay -u everytime you have updated difmplay.
-h | | Display the help message. It's practically the same as the usage information on this web page.
-r | record_file | File to record the audio stream to. Use `""` as an empty argument to specify auto file name generation. [ default format: `YYMMDDHHmmSS-(channel)-(website)(bitrate).wav` ]

## Configuration file
difmplay reads the default settings from `~/.difmplayrc` if it exists.

It must be a sh script that sets zero or more variables listed below. If the same setting is set on both command line and in configuration file, the value on the command line takes effect.

Variable | Description
----|----
`BITRATE` | sets the default bitrate just like the -b option.
`PREMIUM` | sets the premium ID just like the -i option.
`PLAYER` | sets the audio player application to use just like the -p option. Do NOT include player options here
`PLAYER_OPTS` | Additional options to be passed to the player.
`MENU` | indicates if interactive menu should be displayed. Valid values are yes and no. The default is to not show the menu.
`CHANNEL` | sets the default channel to play or preselect from the menu if no channel is specified on the command line.
`RECORD` | `"yes"` or `"no"` Enable or disable records.
`RECORD_PATH` | The file path to use for recording. Overridden from the commandline with -r. Set to `""` to use auto templating, defined below. This setting overrides ANY other RECORD_PATH option that might be defined.
`RECORD_PATH_PREFIX` | The path/folder in which the file needs to be store. ALWAYS ends in `/`
`RECORD_PATH_SUFFIX` | The file format of the file. Do NOT specify a leading `.`
`RECORD_PATH_FMT` | A shell executable line defining how the file name should be generated. It's only generated once on startup. Only variables defined prior in the config file or in the environment are available.
`RECORD_OPTS` | Additional recording options. Note: The last option should ALWAYS be for the filename. Default: `-benchmark -vc null -vo null -novideo -ao pcm:waveheader -ao pcm:fast -ao pcm:file=`
`MY_CHANNELS` | allow defining custom Internet radio channels (outside DI.fm and SKY.fm). MY_CHANNELS must contain a white space separated list of channel names that don't conflict with the predefined DI.fm and SKY.fm channel names.
`MY_URLS` | MY_URLS must contain a white space separated list of playlist URLs matching the channel names.

### Example:
```shell
# Default channel
CHANNEL=eurodance

# Use bigger cache to make sure it doesn't skip.
PLAYER='mplayer'
PLAYER_OPTS='-cache 512 -cache-min 50'

# Never ask what to play via interactive menu.
# This can be overriden with the -n option.
MENU=no
```

## Licensing
difmplay has been put into the public domain. You can do whatever you want with difmplay.

## Download
Put it to some directory in PATH and make it executable (`chmod +x difmplay`). difmplay has been used on GNU/Linux and OpenBSD and probably works on many other operating systems too.

## Dependencies
Some audio player is needed to actually play the streams. Most players should work, including most players with graphical user interface. The player just needs to accept an URL to a .pls playlist as its last command line argument. The default is MPlayer.

* `dialog`: To use interactive menus to select the settings.

* `command`: Displaying multi-column channel list.

* `curl`: Updating the channel list requires curl.
